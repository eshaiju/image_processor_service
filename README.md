# ImageProcessorService
Image Processor Service is an wrapper for is Image Processor API for image storage and processing

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'image_processor_service', git: 'ssh://git@github.com/eshaiju/image_processor_service.git'
```
And then execute:

    $ bundle

## Usage

Fetch an image by ID
````
ImageProcessorService::Client.fetch_image("6a758730-a3f6-4fa9-b85d-4da7e5287a3c")
````
Upload an image
````
# file
ImageProcessorService::Client.upload_image({ file: "/Users/eshaiju/Desktop/login-screen.jpeg" })

#remote_url
ImageProcessorService::Client.upload_image({ file: "http://rubyonjets.com/img/logos/jets-logo-full.png" })
````
Transform an image
````
ImageProcessorService::Client.transform_image("6a758730-a3f6-4fa9-b85d-4da7e5287a3c", {rotate: '380'})
````
Spec samples
```
{
    "crop" : "200x300+100+50",
    "rotate" : "380",
    "format" : "JPEG",
    "colorspace" : "sRGB",
    "resize" : "1000x1000"
}
```
Bulk Upload Image
```
ImageProcessorService::Client.bulk_upload_image({ files: ["http://rubyonjets.com/img/logos/jets-logo-full.png", "https://rubyonjets.com/img/home/jets-web-architecture.png", "/Users/eshaiju/Desktop/login-screen.jpeg"] })
```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/image_processor_service.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
