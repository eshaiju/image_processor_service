require 'typhoeus'
require 'json'
require 'dotenv/load'

module ImageProcessorService
  class BulkUploadImage
    attr_reader :params, :requests
    ENDPOINT_URL = ENV['API_ENDPOINT']

    def initialize(params)
      @params = params
    end

    def run
      api_request.run
    end

    def processed_response
      requests.map { |request| JSON.parse(request.response.body) }
    rescue JSON::ParserError => e
      "Failure : #{e.message}"
    end

    private

    def api_request
      hydra = Typhoeus::Hydra.new

      @requests = params[:files].map { |file|
        request = Typhoeus::Request.new(
            ENDPOINT_URL,
            method: :post,
            body: request_body(file)
        )
        hydra.queue(request)
        request
      }
      hydra
    end

    def request_body(file)
      {
          file: remote_url?(file)? file: File.open(file, 'r')
      }
    end

    def remote_url?(file)
      return false unless file.is_a?(String)

      uri = URI.parse(file)
      %w[http https].include?(uri.scheme)
    end
  end
end