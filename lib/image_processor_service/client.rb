require 'image_processor_service/fetch_image'
require 'image_processor_service/upload_image'
require 'image_processor_service/transform_image'
require 'image_processor_service/bulk_upload_image'

module ImageProcessorService
  class Client
    class << self
      def fetch_image(id)
        image_api = ImageProcessorService::FetchImage.new(id)
        image_api.run
        image_api.processed_response
      end

      def upload_image(params)
        upload_image = ImageProcessorService::UploadImage.new(params)
        upload_image.run
        upload_image.processed_response
      end

      def transform_image(id, specs)
        upload_image = ImageProcessorService::TransformImage.new(id, specs)
        upload_image.run
        upload_image.processed_response
      end

      def bulk_upload_image(params)
        upload_image = ImageProcessorService::BulkUploadImage.new(params)
        upload_image.run
        upload_image.processed_response
      end
    end
  end
end