require 'typhoeus'
require 'json'
require 'dotenv/load'

module ImageProcessorService
  class UploadImage
    attr_reader :params
    ENDPOINT_URL = ENV['API_ENDPOINT']

    def initialize(params)
      @params = params
    end

    def run
      api_request.run
    end

    def processed_response
      JSON.parse(api_request.response.body)
    rescue JSON::ParserError => e
      "Failure : #{e.message}"
    end

    private

    def api_request
      @api_request ||= Typhoeus::Request.new(
        ENDPOINT_URL,
        method: :post,
        body: request_body
      )
    end

    def request_body
      {
        file: remote_url?? params[:file]: File.open(params[:file], 'r')
      }
    end

    def remote_url?
      return false unless params[:file].is_a?(String)

      uri = URI.parse(params[:file])
      %w[http https].include?(uri.scheme)
    end
  end
end
