require 'typhoeus'
require 'json'
require 'dotenv/load'

module ImageProcessorService
  class TransformImage
    attr_reader :specs, :id
    ENDPOINT_URL = ENV['API_ENDPOINT']

    def initialize(id, specs)
      @specs = specs
      @id = id
    end

    def run
      api_request.run
    end

    def processed_response
      JSON.parse(api_request.response.body)
    rescue JSON::ParserError => e
      "Failure : #{e.message}"
    end

    private

    def api_request
      @api_request ||= Typhoeus::Request.new(
          url,
          method: :post,
          body: request_body,
          headers: headers
      )
    end

    def url
      ENDPOINT_URL + id + '/transform'.freeze
    end

    def request_body
      {
          specs: specs
      }.to_json
    end

    def headers
      {
          'Content-Type' => 'application/json'
      }
    end
  end
end