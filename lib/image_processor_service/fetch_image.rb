require 'typhoeus'
require 'json'
require 'dotenv/load'

module ImageProcessorService
  class FetchImage
    attr_reader :id
    ENDPOINT_URL = ENV['API_ENDPOINT']

    def initialize(id)
      @id = id
    end

    def run
      api_request.run
    end

    def processed_response
      JSON.parse(api_request.response.body)
    rescue JSON::ParserError => e
      "Failure : #{e.message}"
    end

    private

    def api_request
      @api_request ||= Typhoeus::Request.new(
        ENDPOINT_URL + id,
        method: :get,
        headers: headers
      )
    end

    def headers
      {
        'Content-Type' => 'application/json'
      }
    end
  end
end